# satnogs-config image
#
# Copyright (C) 2022-2025 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

ARG PYTHON_IMAGE_TAG=3.12-alpine3.21

FROM python:${PYTHON_IMAGE_TAG} AS requirements
LABEL org.opencontainers.image.authors='SatNOGS project <dev@satnogs.org>'

# Disable pip cache, new version check and root user warning
ARG PIP_NO_CACHE_DIR=true
ARG PIP_DISABLE_PIP_VERSION_CHECK=true
ARG PIP_ROOT_USER_ACTION=ignore

# Disable Python compilation to bytecode
ENV PYTHONDONTWRITEBYTECODE=1

# Disable output buffering
ENV PYTHONUNBUFFERED=1

# Install system packages
RUN --mount=type=bind,source=packages.alpine,target=/usr/local/src/satnogs-config/packages.alpine \
	xargs -r -a /usr/local/src/satnogs-config/packages.alpine apk add --no-cache

# Build Python dependencies wheels
RUN --mount=type=bind,source=requirements.txt,target=/usr/local/src/satnogs-config/requirements.txt \
	pip wheel \
		-w /tmp/wheelhouse \
		--no-deps \
		--prefer-binary \
		-r /usr/local/src/satnogs-config/requirements.txt


FROM python:${PYTHON_IMAGE_TAG} AS satnogs-config
LABEL org.opencontainers.image.authors='SatNOGS project <dev@satnogs.org>'

# Disable pip cache, new version check and root user warning
ARG PIP_NO_CACHE_DIR=true
ARG PIP_DISABLE_PIP_VERSION_CHECK=true
ARG PIP_ROOT_USER_ACTION=ignore

# Disable Python compilation to bytecode
ENV PYTHONDONTWRITEBYTECODE=1

# Disable output buffering
ENV PYTHONUNBUFFERED=1

# Install system packages
RUN --mount=type=bind,source=packages.alpine,target=/usr/local/src/satnogs-config/packages.alpine \
	xargs -r -a /usr/local/src/satnogs-config/packages.alpine apk add --no-cache

# Build Python application wheel
COPY . /usr/local/src/satnogs-config
RUN pip wheel \
	-w /tmp/wheelhouse \
	--no-deps \
	/usr/local/src/satnogs-config


FROM python:${PYTHON_IMAGE_TAG}
LABEL org.opencontainers.image.authors='SatNOGS project <dev@satnogs.org>'

# Disable pip cache, new version check and root user warning
ARG PIP_NO_CACHE_DIR=true
ARG PIP_DISABLE_PIP_VERSION_CHECK=true
ARG PIP_ROOT_USER_ACTION=ignore

# Disable Python compilation to bytecode
ENV PYTHONDONTWRITEBYTECODE=1

# Disable output buffering
ENV PYTHONUNBUFFERED=1

ARG SATNOGS_CONFIG_UID=500
ARG SATNOGS_CONFIG_VARSTATEDIR=/var/lib/satnogs-config

# Add unprivileged system user
RUN addgroup \
		-S \
		-g ${SATNOGS_CONFIG_UID} \
		satnogs-config \
	&& adduser \
		-S \
		-g satnogs-config \
		-H \
		-u ${SATNOGS_CONFIG_UID} \
		-G satnogs-config \
		-h ${SATNOGS_CONFIG_VARSTATEDIR} \
		satnogs-config

# Create application varstate directory
RUN install -d -m 1777 -o ${SATNOGS_CONFIG_UID} -g ${SATNOGS_CONFIG_UID} ${SATNOGS_CONFIG_VARSTATEDIR}

# Install system packages
RUN --mount=type=bind,source=packages.alpine,target=/usr/local/src/satnogs-config/packages.alpine \
	grep -v "^\(gcc\|musl-dev\|linux-headers\|git\)$" /usr/local/src/satnogs-config/packages.alpine | xargs -r apk add --no-cache

# Install Python dependencies
RUN --mount=type=bind,source=requirements.txt,target=/usr/local/src/satnogs-config/requirements.txt \
	--mount=type=bind,source=/tmp/wheelhouse,target=/tmp/wheelhouse,from=requirements \
	pip install \
		--no-deps \
		--only-binary :all: \
		--no-index \
		-f /tmp/wheelhouse \
		-r /usr/local/src/satnogs-config/requirements.txt

# Install Python application
RUN --mount=type=bind,source=/tmp/wheelhouse,target=/tmp/wheelhouse,from=satnogs-config \
	pip install \
		--no-deps \
		--only-binary :all: \
		--no-index \
		-f /tmp/wheelhouse \
		satnogs-config

# Create directories and volumes
VOLUME "${SATNOGS_CONFIG_VARSTATEDIR}"

CMD ["satnogs-config"]
