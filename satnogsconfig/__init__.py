"""SatNOGS Config module initialization"""
import importlib.resources
import sys

from satnogsconfig import settings
from satnogsconfig.config import Config
from satnogsconfig.menu import Menu

from ._version import get_versions

__version__ = get_versions()['version']

del get_versions

MENU_FILENAME = 'menu.yml'


def main():
    """SatNOGS Setup utility"""

    config = Config(settings.CONFIG_FILE)
    menu = Menu(importlib.resources.open_text(__name__, MENU_FILENAME), config)
    try:
        menu.show()
    except KeyboardInterrupt:
        sys.exit(0)
